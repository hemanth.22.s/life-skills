## What is Sexual harassment?
Sexual harassment includes unwelcome sexual advances, requests for sexual favors, and other verbal or physical harassment of a sexual nature in the workplace or learning environment.

* <b>Quid pro quo harassment:</b> occurs when a supervisor's request for sexual favors or other sexual conduct results in a tangible job action. Examples include "I'll give you the promotion if you sleep with me" or "I'll fire you unless you go out with me."

* <b>Hostile work environment:</b> occurs when an employee is subjected to unwelcome physical or verbal conduct of a sexual nature that is so severe or pervasive as to alter the employee's working conditions or create an abusive work environment.

While quid pro quo harassment is relatively straightforward, hostile work environment claims can be more difficult to detect. What types of behaviors qualify as harassment? How much is enough to qualify as harassment? We provide some guidance below.

## What kinds of behaviour cause sexual harassment?
* Consistent compliments about an employee's appearance.
* Commenting on the attractiveness of others in front of an employee.
* Discussing one's sexual life in front of an employee.
* Asking an employee about his or her sexual life.
* Disseminating naked or bikini-clad women or shirtless men in the workplace.
* Making sexual jokes.
* Sending sexually suggestive text messages or emails.
* Leaving unwanted gifts of a sexual or romantic nature.
* Spreading sexual rumours about a coworker.
* Repeated hugs or other unwanted touching (such as a hand on an employee's back).

## What would you do in case you face or witness any incident or repeated incidents of such behaviour?
While each person needs to decide what action plan works best for him or herself, many individuals have found that informal action facilitates the fastest resolution with the fewest complications. You can start by telling the person involved to stop the behavior. Try to be as clear as possible. For example, "It makes me uncomfortable when you rub my shoulders; please do not do this." If this does not work, you should consider putting it in writing and telling the person what conduct you find offensive and what action you will take if it continues. For instance, "I find your sexual jokes offensive." "I consider these to be sexual harassment, and I will file a complaint if you continue to tell me about them." Date and sign the letter, keep a copy, and have a witness watch you give it to the offender.
If none of the above works, tell your supervisor (unless he or she is the offender) or a human resource person in your organisation (i.e., file a complaint). Check to see if your organisation has a mediation or informal complaint resolution process. Cooperate with any investigation and document all that has happened.Read and understand your organization's sexual harassment policy.Understand what behaviour constitutes sexual harassment.Conduct ongoing sexual harassment education for your employees, ensuring that they understand the sexual harassment policy and how to report sexual harassment.Monitor the conduct and environment of the workplace.Encourage comments regarding the work environment, including problems regarding sexual harassment.

Let your employees know that you will not tolerate sexual harassment at the workplace and demonstrate your commitment to "zero-tolerance" by taking immediate action when appropriate.Post the sexual harassment policy in a prominent place, distribute the policy to all employees, and suggest discussing it in a staff meeting.Be both neutral and objective during an investigation of an incident.
During the investigation of a complaint and possible subsequent discipline of the harasser, co-workers may feel angry or threatened by the complainant and his or her supporters. Stop rumours and offensive actions by coworkers immediately if an incident occurs. It is important to demonstrate that this type of activity will not be tolerated.If tension between coworkers is a problem, consider having a workshop on team building or communication (not, however, about a particular incident!)
